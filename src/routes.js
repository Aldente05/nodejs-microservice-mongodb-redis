/**
 * @author fputra on 01/08/21
 */
const express = require('express')
const router = express.Router()
const UserController = require('./controllers/UserController')

router.use('/v1/user', UserController)

module.exports = router
