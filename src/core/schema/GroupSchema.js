/**
 * @author fputra on 07/02/22
 */
let sequelize = require('../RepositoryConfig');
const Sequelize = require('sequelize');

const group = sequelize.define('group', {
    id :{
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey : true
    },
    ParentObjectAlias:{type: Sequelize.STRING},
    ParentObjectName:{type: Sequelize.STRING},
    JobGroup:{type: Sequelize.STRING}
});
module.exports = group;
