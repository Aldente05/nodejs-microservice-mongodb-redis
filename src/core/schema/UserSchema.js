/**
 * Created by f.putra on 2019-07-30.
 */
let sequelize = require('../RepositoryConfig');
let organisation_unit = require('../schema/OrganisationUnitSchema');
let group = require('../schema/GroupSchema');
const Sequelize = require('sequelize');

const user = sequelize.define('user', {
    id :{
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey : true
    },
    objectName:{type: Sequelize.STRING },
    login:{type: Sequelize.STRING },
    email:{type: Sequelize.STRING },
    noHp:{type: Sequelize.STRING },
    namaRekening:{type: Sequelize.STRING },
    noRekening:{type: Sequelize.STRING },
    orgDepartment:{type: Sequelize.STRING },
    orgPosition:{type: Sequelize.STRING },
    privyID:{type: Sequelize.STRING }
});
user.hasMany(organisation_unit)
user.hasMany(group)
module.exports = user;
