/**
 * Created by f.putra on 2019-05-02.
 */
let User = require('../core/schema/UserSchema')
let org = require('../core/schema/OrganisationUnitSchema')
let groups = require('../core/schema/GroupSchema')
let jwt = require('jsonwebtoken');
let crypto = require('crypto');

let userService = {
    login: async (payload) => {
        this.salt = crypto.randomBytes(16).toString('hex');
        let user = await User.findOne({
            where: {
                email: payload.emailAddress
            },
            returning: true
        })

        if (user === null) {
            return {code: 400, message: "User not found."}
        } else {
            let hash = crypto.pbkdf2Sync(payload.password, user.salt, 1000, 64, `sha512`).toString(`hex`);
            if (hash === user.hash) {
                const token = jwt.sign({
                    sub: user.id,
                    userName: user.userName,
                }, JSON.parse(process.env.JWT_CONFIG).secret);
                return {
                    code: 200,
                    message: {
                        id: user.id,
                        token: token,
                        userName: user.userName,
                        emailAddress: user.emailAddress,
                        accountNumber: user.accountNumber,
                        identityNumber: user.identityNumber
                    }
                }
            } else {
                return {code: 403, message: "Wrong Password"}
            }
        }
    },

    register: async (body) => {
        console.log({
            objectName:body.objectName,
            login:body.login,
            email:body.email,
            noHp:body.noHp,
            namaRekening:body.namaRekening,
            noRekening:body.noRekening,
            orgDepartment:body.orgDepartment,
            orgPosition:body.orgPosition,
            privyID:body.privyID,
            groups: body.group.map(res =>({
                ParentObjectAlias: res.ParentObjectAlias,
                ParentObjectName: res.ParentObjectName,
                JobGroup: res.JobGroup
            })),
            // orgs: body.organisasiUnit
        })
        return new Promise((resolve, reject) => {
            User.create({
                objectName:body.objectName,
                login:body.login,
                email:body.email,
                noHp:body.noHp,
                namaRekening:body.namaRekening,
                noRekening:body.noRekening,
                orgDepartment:body.orgDepartment,
                orgPosition:body.orgPosition,
                privyID:body.privyID,
                groups: body.group,
                orgs: body.organisasiUnit
            },{
                include: [groups, org]
            }).then(resolve).catch(reject)
        })
    },

    updateProfile: async (body) => {
        delete body.id;
        return User.findOneAndUpdate({emailAddress: body.emailAddress}, body).exec()
    },
    findByAccountNumber: (payload) => {
        return User.findOne({accountNumber: payload}, "userName accountNumber emailAddress identityNumber Id createdAt updatedAt").exec()
    },
    findByIdentityNumber: async (payload) => {
        return User.findOne({identityNumber: payload}, "userName accountNumber emailAddress identityNumber Id createdAt updatedAt").exec()
    },

    getAll: async (offset, limit) => {
        return User.findAll();
    },

    deleteById: (id) => {
        return User.destroy({
            where: {
                id: id
            }
        })
    }
};
module.exports = userService;
