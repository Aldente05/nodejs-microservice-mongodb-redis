const {Op} = require('sequelize');

module.exports = {
    apps: [{
        name: 'Nodejs Microservice',
        script: 'swagger.js',
        instances: 1,
        exec_mode: "fork",
        watch: true,
        error_file: "./logs/err.log",
        out_file: "./logs/out.log",
        merge_logs: true,
        log_date_format: "YYYY-MM-DD HH:mm Z",
        ignore_watch: [".idea", "node_modules", ".git", "logs", "swagger_output.json"],
        env_development: {
            LISTEN_PORT: 3200,
            NODE_ENV: 'development',
            JWT_CONFIG: JSON.stringify({
                secret: '172kjdhsfgSH6123SJDHF1200#$@%7jasbdha1@!#&$*dshdjka126S!@^kskjd1ASKJDH!@@&*DSAGD@L',
                algorithms: ['RS256']
            }),
            CONFIG_DB: JSON.stringify({
                username: 'root',
                password: 'rakkuma5758',
                database: 'adira_db',
                host: '151.106.113.54',
                port: '5432',
                dialect: 'postgres',
                dialectOptions: {
                    dateStrings: true,
                    typeCast: true,
                },
                timezone: '+07:00',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            })
        },
        env_staging: {
            LISTEN_PORT: 3200,
            NODE_ENV: 'staging',
            JWT_CONFIG: JSON.stringify({
                secret: '172kjdhsfgSH6123SJDHF1200#$@%7jasbdha1@!#&$*dshdjka126S!@^kskjd1ASKJDH!@@&*DSAGD@L',
                algorithms: ['RS256']
            }),
            CONFIG_DB: JSON.stringify({
                username: 'root',
                password: 'root',
                database: 'adira',
                host: '151.106.113.54',
                port: '5432',
                dialect: 'postgres',
                dialectOptions: {
                    dateStrings: true,
                    typeCast: true,
                },
                timezone: '+07:00',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            })
        },
        env_production: {
            LISTEN_PORT: 3200,
            NODE_ENV: 'production',
            JWT_CONFIG: JSON.stringify({
                secret: '172kjdhsfgSH6123SJDHF1200#$@%7jasbdha1@!#&$*dshdjka126S!@^kskjd1ASKJDH!@@&*DSAGD@L',
                algorithms: ['RS256']
            }),
            CONFIG_DB: JSON.stringify({
                username: 'root',
                password: 'root',
                database: 'adira',
                host: '151.106.113.54',
                port: '5432',
                dialect: 'postgres',
                dialectOptions: {
                    dateStrings: true,
                    typeCast: true,
                },
                timezone: '+07:00',
                pool: {
                    max: 20,
                    min: 0,
                    acquire: 30000,
                    idle: 1000
                },
                logging: false,
                operatorsAliases: {
                    $and: Op.and,
                    $or: Op.or,
                    $eq: Op.eq,
                    $gt: Op.gt,
                    $lt: Op.lt,
                    $lte: Op.lte,
                    $like: Op.like
                }
            })
        }
    }]
};
